resource "aws_instance" "govtech_counter_bastion_host" {
  instance_type     = var.instance_type
  ami               = var.image_id
  subnet_id         = aws_subnet.govtech_counter_public_subnet_1c_natgateway.id    # NAT Gateway is located at "public-subnet-1c-natgateway".
  security_groups   = [aws_security_group.govtech_counter_bastion_host_sg.id]
  key_name          = aws_key_pair.govtech_counter_asg_ssh_key.key_name
  disable_api_termination = false
  ebs_optimized     = false
  root_block_device {
    volume_size = "10"
  }
  tags = {
    "Name" = var.govtech_counter_bastion_host_name
  }
}

resource "aws_eip" "govtech_counter_bastion_host_eip" {
  instance = aws_instance.govtech_counter_bastion_host.id
  vpc = true
}

resource "aws_security_group" "govtech_counter_bastion_host_sg" {
  name        = var.govtech_counter_bastion_host_sg_name
  description = "GovTech-Counter Bastion Host Security Group."
  vpc_id      =  var.vpc_id

  ingress {
    description      = "Restrict inbound access for public ALB to only allow on port 80/TCP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "Restrict inbound access for public ALB to only allow on port 443/TCP"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "Allow SSH from specific IP range."
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}