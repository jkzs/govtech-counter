sudo su -

# Yum
yum install yum-utils -y
cat > /etc/yum.repos.d/nginx.repo <<EOF
[nginx-stable]
name=nginx stable repo
baseurl=http://nginx.org/packages/amzn2/2/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true

[nginx-mainline]
name=nginx mainline repo
baseurl=http://nginx.org/packages/mainline/amzn2/2/x86_64/
gpgcheck=1
enabled=0
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true
EOF
yum-config-manager --enable nginx-mainline
yum install -y nginx
nginx -v
systemctl start nginx
systemctl enable nginx
systemctl status nginx
curl -I 127.0.0.1

# Copy
aws s3 cp s3://${aws_s3_bucket.govtech_counter_assets_bucket.id}/* /usr/share/nginx/html/
# aws s3 cp s3://govtech-counter-assets/index.html /usr/share/nginx/html/