# Restrict inbound access to ALB
module "govtech_counter_alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"

  name = var.govtech_counter_alb_name

  load_balancer_type = var.govtech_counter_lb_type

  vpc_id             = var.vpc_id
  subnets            = ["${var.public_subnet_id_string}","${aws_subnet.govtech_counter_public_subnet_1b.id}","${aws_subnet.govtech_counter_public_subnet_1c.id}"]
  security_groups    = ["${aws_security_group.govtech_counter_alb_sg.id}"]

#   access_logs = {
#     bucket = "my-alb-logs"
#   }

  target_groups = [
    {
      name_prefix      = "alb-tg"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
    #   targets = {
    #     my_target = {
    #       target_id = "i-0123456789abcdefg"    # How to put Auto Scaling Group here?
    #       port = 80
    #     }
    #     my_other_target = {
    #       target_id = "i-a1b2c3d4e5f6g7h8i"
    #       port = 8080
    #     }
    #   }
    }
  ]

#   https_listeners = [
#     {
#       port               = 443
#       protocol           = "HTTPS"
#       certificate_arn    = "arn:aws:iam::123456789012:server-certificate/test_cert-123456789012"
#       target_group_index = 0
#     }
#   ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]
}

resource "aws_security_group" "govtech_counter_alb_sg" {
  name        = var.govtech_counter_alb_sg_name
  description = "GovTech-Counter Application LoadBalancer Security Group."
  vpc_id      =  var.vpc_id

  ingress {
    description      = "Restrict inbound access for public ALB to only allow on port 80/TCP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    description      = "Allow outbound access to ASG machines."
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    security_groups  = ["${aws_security_group.govtech_counter_asg_sg.id}"]
  }
}