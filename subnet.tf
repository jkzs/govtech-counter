resource "aws_subnet" "govtech_counter_private_subnet_1a" {
  vpc_id     = var.vpc_id
  cidr_block = var.private_subnet_cidr_block
  map_public_ip_on_launch = false

  tags = {
    Name = var.private_subnet_1a_name
  }
}

resource "aws_route_table" "govtech_counter_private_rt" {
  vpc_id = var.vpc_id

  route {
    cidr_block = var.govtech_counter_nat_gateway_rt_cidr_block
    nat_gateway_id = aws_nat_gateway.govtech_counter_nat_gateway.id
  }
}

resource "aws_route_table_association" "govtech_counter_private_rt_association_1a" {
  subnet_id      = aws_subnet.govtech_counter_private_subnet_1a.id
  route_table_id = aws_route_table.govtech_counter_private_rt.id
}

# public-subnet-1b
resource "aws_subnet" "govtech_counter_public_subnet_1b" {
  vpc_id            = var.vpc_id
  cidr_block        = var.public_subnet_1b_cidr_block
  availability_zone = var.public_subnet_1b_az
  map_public_ip_on_launch = true

  tags = {
    Name = var.public_subnet_1b_name
  }
}

resource "aws_route_table_association" "govtech_counter_public_rt_association_1b" {
  subnet_id      = aws_subnet.govtech_counter_public_subnet_1b.id
  route_table_id = var.govtech_counter_public_rt_id
}

# public-subnet-1c
resource "aws_subnet" "govtech_counter_public_subnet_1c" {
  vpc_id            = var.vpc_id
  cidr_block        = var.public_subnet_1c_cidr_block
  availability_zone = var.public_subnet_1c_az
  map_public_ip_on_launch = true

  tags = {
    Name = var.public_subnet_1c_name
  }
}

resource "aws_route_table_association" "govtech_counter_public_rt_association_1c" {
  subnet_id      = aws_subnet.govtech_counter_public_subnet_1c.id
  route_table_id = var.govtech_counter_public_rt_id
}