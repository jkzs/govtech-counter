
provider "aws" {
  region = var.region

  default_tags {
    tags = {
      project     = var.project
      createdBy   = var.createdBy
      provisionBy = var.provisionBy
    }
  }
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "7.0.0"
  # insert the 4 required variables here
}

# Push/save .tfstate to S3
terraform {
  backend "s3" {
    bucket = "govtech-tf"
    key = "tfstates/general/govtech-counter.tfstate"
    region = "ap-southeast-1"
    access_key = "AKIASETG2MCXTZ3LJYYZ"
    secret_key = "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"
  }
}