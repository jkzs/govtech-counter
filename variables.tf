# VPC details
variable "vpc_id" {
  default = "vpc-0c37b5756a14d639f"
}

# Private Subnet CIDR block
variable "private_subnet_1a_name" {
  default = "private-subnet-1a"
  description = "Name for newly Terraform-provisioned Private Subnet."
}

variable "private_subnet_cidr_block" {
  default = "172.31.48.0/20"
  description = "Randomly assigned/given CIDR block."
}

variable "private_rt_name" {
  default = "172.31.48.0/20"
  description = "Randomly assigned/given CIDR block."
}

# Public Subnet
variable "public_subnet_id_array" {
  type    = list(string)
  default = ["subnet-01a6486d6d07d3a6f"]
  description = "Provisioned by default upon VPC's creation." # In List/Array format.
}

variable "public_subnet_id_string" {
  default = "subnet-01a6486d6d07d3a6f"
  description = "Provisioned by default upon VPC's creation." # In String format.
}

# public-subnet-1b
variable "public_subnet_1b_name" {
  default = "public-subnet-1b"
  description = "Name for newly Terraform-provisioned Public Subnet."
}

variable "public_subnet_1b_cidr_block" {
  default = "172.31.32.0/20"
  description = "Randomly assigned/given CIDR block, for newly Terraform-provisioned Public Subnet."
}

variable "public_subnet_1b_az" {
  default = "ap-southeast-1b"
  description = "Availability zone for newly Terraform-provisioned Public Subnet."
}

# public-subnet-1c
variable "public_subnet_1c_name" {
  default = "public-subnet-1c"
  description = "Name for newly Terraform-provisioned Public Subnet."
}

variable "public_subnet_1c_cidr_block" {
  default = "172.31.64.0/20"
  description = "Randomly assigned/given CIDR block, for newly Terraform-provisioned Public Subnet."
}

variable "public_subnet_1c_az" {
  default = "ap-southeast-1c"
  description = "Availability zone for newly Terraform-provisioned Public Subnet."
}

# public-subnet-1c-natgateway
variable "public_subnet_1c_natgateway_name" {
  default = "public-subnet-1c-natgateway"
  description = "Name for newly Terraform-provisioned Public Subnet."
}

variable "public_subnet_1c_natgateway_cidr_block" {
  default = "172.31.80.0/20"
  description = "Randomly assigned/given CIDR block, for newly Terraform-provisioned Public Subnet."
}

variable "public_subnet_1c_natgateway_az" {
  default = "ap-southeast-1c"
  description = "Availability zone for newly Terraform-provisioned Public Subnet."
}

variable "govtech_counter_public_rt_id" {
  default = "rtb-0fa6937613d833a16"
  description = "Availability zone for newly Terraform-provisioned Public Subnet."
}

# Default tags
variable "region" {
  default = "ap-southeast-1"
}

variable "createdBy" {
  default = "jason.khor"
}

variable "provisionBy" {
  default = "Terraform"
}

variable "project" {
  default = "GovTech-Counter"
  description = "Project name."
}

# Assets S3 Bucket config
variable "s3_assets_bucket_name" {
  default = "govtech-counter-assets"
  description = "GovTech-Counter assets bucket name."
}

variable "s3_assets_bucket_acl" {
  default = "private"
}

variable "s3_assets_bucket_versioning" {
  default = "true"
}

variable "s3_assets_bucket_log_target_prefix" {
  default = "log/"
}

variable "s3_assets_log_bucket_name" {
  default = "govtech-counter-assets-log-bucket"
  description = "GovTech-Counter assets log bucket name, for bucket logging purposes."
}

variable "s3_assets_log_bucket_acl" {
  default = "log-delivery-write"
}

variable "govtech_counter_homepage_object_key" {
  default = "index.html"
  description = "Name of object."
}

variable "govtech_counter_homepage_object_source" {
  default = "assets/index.html"
  description = "Path to/directory of object."
}

# Upload AmazonSSM and NGiNX Packages to S3
# variable "govtech_counter_amazonssm_object_key" {
#   default = "amazon-ssm-agent.rpm"
#   description = "Name of object."
# }

# variable "govtech_counter_amazonssm_object_source" {
#   default = "archive/amazon-ssm-agent.rpm"
#   description = "Path to/directory of object."
# }

# variable "govtech_counter_nginxpackage_object_key" {
#   default = "nginx-1.23.1.tar.gz"
#   description = "Name of object."
# }

# variable "govtech_counter_nginxpackage_object_source" {
#   default = "archive/nginx-1.23.1.tar.gz"
#   description = "Path to/directory of object."
# }

# ASG Config
variable "govtech_counter_role_policy_name" {
  default = "govtech-counter-role-policy"
}

variable "asg_desired_capacity" {
  default = "3"
}

variable "asg_max_size" {
  default = "3"
}

variable "asg_min_size" {
  default = "1"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "asg_health_check_grace_period" {
  default = "300"
}

variable "asg_health_check_type" {
  default = "ELB"
}

variable "asg_force_delete" {
  default = "true"
}

variable "asg_key_name" {
  default = "govtech-counter"
}

variable "asg_public_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCgbWsYW/Kp8pFcIntVfds6IBy75MztfRFPb5BfYMufrg5U1CdbgcDrhohcg0ojO9P0R8wzaIpHK+kiXPc6Lzxyt3vrcNRBUGISUUePHBGPq6nG8ihJZ5HfqwV4DW4nL0eHYkyD8WdSSCpx3cw104wn8BlqiVi1GqDvXn9jne3VBVPg0o70h7ZLj0jjPrgZRFNQs/iSZ+UpBVkH8EWMREl6E8ALQ44t6oQUxTMg8JDYnv0szy9xmLTjRWiJu+MQJqpEpmGNZZR9YT+QU8+YftCfLv8JMkjBh7xSpG9e9nPSEHo5cDYVK3bgAvcdyXbUc9ePU47sZCPgqFJSbmaX7fqB"
  description = "Generated from private key." # via "ssh-keygen -y -f govtech-counter" command
}

variable "image_id" {
  default = "ami-0ff89c4ce7de192ea"
  description = "Amazon Linux 2 Kernel 5.10 AMI 2.0.20220719.0 x86_64 HVM gp2"
}

variable "govtech_counter_asg_instance_profile_name" {
  default = "govtech-counter-asg-instance-profile"
}

variable "launch_template_name_prefix" {
  default = "govtech-counter-launch-template"
}

variable "launch_template_version" {
  default = "$Latest"
  description = "Launch template version to use (always use latest for now)."
}

variable "govtech_counter_asg_sg_name" {
  default = "govtech-counter-asg-sg"
}

variable "govtech_counter_asg_role_name" {
  default = "govtech-counter-asg-role"
}

variable "govtech_counter_bastion_host_sg_name" {
  default = "govtech-counter-bastion-host-sg"
}

variable "govtech_counter_bastion_host_name" {
  default = "govtech-counter-bastion-host"
}

# variable "asg_public_subnet_id" {
#   type    = list(string)
#   default = ["subnet-01a6486d6d07d3a6f","${aws_subnet.govtech_counter_public_subnet.id}"]
#   description = "Provisioned by default upon VPC's creation."
# }

# ALB Config
variable "govtech_counter_alb_name" {
  default = "govtech-counter-alb"
}

variable "govtech_counter_lb_type" {
  default = "application"
}

# ALB Security Group Config
variable "govtech_counter_alb_sg_name" {
  default = "govtech-counter-alb-sg"
}

# NAT Gateway Config
variable "govtech_counter_nat_gateway_name" {
  default = "govtech-counter-nat-gateway"
}

variable "govtech_counter_nat_gateway_rt_cidr_block" {
  default = "0.0.0.0/0"
}