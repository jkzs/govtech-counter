resource "aws_eip" "govtech_counter_nat_gateway_eip" {
  vpc = true
}

# Putting NAT Gateway at "public-subnet-1c-natgateway"
resource "aws_nat_gateway" "govtech_counter_nat_gateway" {
  allocation_id = aws_eip.govtech_counter_nat_gateway_eip.id
  subnet_id = aws_subnet.govtech_counter_public_subnet_1c_natgateway.id
  tags = {
    "Name" = var.govtech_counter_nat_gateway_name
  }
}

output "nat_gateway_ip" {
  value = aws_eip.govtech_counter_nat_gateway_eip.public_ip
}

# public-subnet-1c-natgateway
resource "aws_subnet" "govtech_counter_public_subnet_1c_natgateway" {
  vpc_id            = var.vpc_id
  cidr_block        = var.public_subnet_1c_natgateway_cidr_block
  availability_zone = var.public_subnet_1c_natgateway_az
  map_public_ip_on_launch = true

  tags = {
    Name = var.public_subnet_1c_natgateway_name
  }
}

resource "aws_route_table_association" "govtech_counter_public_rt_association_1c_natgateway" {
  subnet_id      = aws_subnet.govtech_counter_public_subnet_1c_natgateway.id
  route_table_id = var.govtech_counter_public_rt_id
}