[[_TOC_]]

# Scenario 1 - Terraform, AWS, CICD
## Bugs/Defects
1. ~~Fix start up script (e.g. `user_data`) for automating NGiNX installation and setup upon booting.~~ [FIXED]
    - ~~Not working currently.~~
    - ~~Manually done via remotely SSH from Bastion host into each of the ASG machines.~~
2. Unable to do string replacement (i.e. Capitalise letters in `assets/index.html`) via GitLab CI pipeline.
    - Somehow GitLab CI would instead add `U` in front of the letters
        - ![Y U DU TIZ](img/UIncrement.png)
3. ALB module would complain about not using enough Subnets despite creation is successful.
    - ![At least two subnets in two different Availability Zones must be specified.](img/insufficient-subnets-for-alb.png)
4. ASG's machines are registered as ALB's target group manually currently.
    - How to register ASG as ALB's target group via Terraform ALB module?
    - Done manually currently.

## Improvement/Future Enhancement
1. Use Ansible to automate NGiNX installation and setup.
2. Enable ALB logging.
3. Use For Loop (e.g. count index) to reduce code duplication while creating multiple same resources.
4. Implement proper NAT Gateway for external internet access, for downloading packages.
5. Alternatively, could've used Beanstalk to provision everything (could've been faster?).

## Screenshots
1. Before:
    - ![First successful provisioning from Terraform on AWS.](img/initial-tf-apply.png)
2. After:
    - ![Y U DU TIZ](img/UIncrement.png)

## Misc.
### Links
1. [Jason Khor / govtech-counter · GitLab](https://gitlab.com/jkzs/govtech-counter)
    - [Pipelines · Jason Khor / govtech-counter · GitLab](https://gitlab.com/jkzs/govtech-counter/-/pipelines)
2. [http://govtech-counter-alb-62048803.ap-southeast-1.elb.amazonaws.com](http://govtech-counter-alb-62048803.ap-southeast-1.elb.amazonaws.com)

# Scenario 2 - Kubernetes
## Step-by-Step
1. Start Minikube:
    - `minikube start --driver=docker --kubernetes-version v1.20.1`
2. `cd docker/`
3. `@FOR /f "tokens=*" %i IN ('minikube -p minikube docker-env --shell cmd') DO @%i`
4. `docker build . -t govtech-counter:latest`
5. Deploy `govtech-counter` Deployment and Service:
    - `kubectl apply -f deployment-service.yaml`
    - `kubectl expose deployment/govtech-counter`
6. Port-forward:
    - `kubectl port-forward service/govtech-counter 8080:80 -n default`
    - Open browser and browse to `http://127.0.0.1:8080`
7. `cd helm-charts`
8. Install MySQL Helm Chart:
    - `helm dependency build mysql-v9.2.6/`
    - `helm upgrade --install mysql mysql-v9.2.6/ -f ./mysql-v9.2.6/values.yaml -n default`
9. Install Prometheus-Node-Exporter Helm Chart:
    - `helm upgrade --install prometheus-node-exporter ./prometheus-node-exporter/ -f ./prometheus-node-exporter/values.yaml -n default`
10. Install Prometheus-MySQL-Exporter Helm Chart:
    - `helm upgrade --install prometheus-mysql-exporter ./prometheus-mysql-exporter/ -f ./prometheus-mysql-exporter/values.yaml -n default`
11. Install Kube-State-Metrics Helm Chart:
    - `helm upgrade --install kube-state-metrics ./kube-state-metrics/ -f ./kube-state-metrics/values.yaml -n default`
12. Install Kube-Prometheus-Stack Helm Chart:
    - `helm repo add grafana https://grafana.github.io/helm-charts`
    - `helm repo add prometheus-community https://prometheus-community.github.io/helm-charts`
    - `helm dependency build ./kube-prometheus-stack/`
    - `helm upgrade --install kube-prometheus-stack ./kube-prometheus-stack/ -f ./kube-prometheus-stack/values.yaml -n default`
13. Install Grafana Helm Chart:
    - `helm upgrade --install grafana ./grafana/ -f ./grafana/values.yaml -n default`
14. Port-forward:
    - `kubectl port-forward service/prometheus-operated 9090:9090 -n default`
    - Open browser and browse to `http://127.0.0.1:9090`
15. Example of monitoring `namespace_cpu:kube_pod_container_resource_limits:sum` on Prometheus:
    - ![namespace_cpu:kube_pod_container_resource_limits:sum](img/image_81.png)


## Bugs/Defects
1. NGiNX web page pod would crash if I tried to append first name into the `/usr/share/nginx/html/index.html` file in the container.
    - I have used `echo` & `sed -i` to do string replacement but they would result in the same as well, `sed -i` would complain about wrong flags/argument though.
    - ![Pod crash](img/image_79.png)
    - ![Container env and command/args to append first name](img/image_80.png)
2. Ran out of time for the following:
    - Unable to find out how to connect NGiNX web page to MySQL DB.
    - Unable to setup Grafana dashboards to properly visual metrics from Prometheus.
    