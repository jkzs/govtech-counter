resource "aws_iam_instance_profile" "govtech_counter_asg_instance_profile" {
  name = var.govtech_counter_asg_instance_profile_name
  role = aws_iam_role.govtech_counter_asg_role.name
}

resource "aws_iam_role" "govtech_counter_asg_role" {
  name = var.govtech_counter_asg_role_name
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "govtech_counter_role_policy" {
  name = var.govtech_counter_role_policy_name
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "s3:*",
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${aws_s3_bucket.govtech_counter_assets_bucket.id}/*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "govtech_counter_role_policy_attachment" {
  role        = aws_iam_role.govtech_counter_asg_role.name
  policy_arn  = aws_iam_policy.govtech_counter_role_policy.arn
}

resource "aws_key_pair" "govtech_counter_asg_ssh_key" {
  key_name    = var.asg_key_name
  public_key  = var.asg_public_key
}

resource "aws_launch_template" "govtech_counter_launch_template" {
  name_prefix             = var.launch_template_name_prefix
  image_id                = var.image_id
  instance_type           = var.instance_type
  key_name                = aws_key_pair.govtech_counter_asg_ssh_key.key_name
  user_data               = filebase64("${path.module}/archive/startup.sh")
  update_default_version  = true

  iam_instance_profile {
    name = aws_iam_instance_profile.govtech_counter_asg_instance_profile.name
  }

  network_interfaces {
    subnet_id       = aws_subnet.govtech_counter_private_subnet_1a.id
    security_groups = ["${aws_security_group.govtech_counter_asg_sg.id}"]
  }
}

resource "aws_security_group" "govtech_counter_asg_sg" {
  name        = var.govtech_counter_asg_sg_name
  description = "GovTech-Counter ASG Security Group."
  vpc_id      =  var.vpc_id

  ingress {
    description      = "Restrict inbound access for public ALB to only allow on port 80/TCP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "Restrict inbound access for public ALB to only allow on port 80/TCP"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "Allow SSH from specific IP range."
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  # egress {
  #   description      = "Allow outbound access to ALB."
  #   from_port        = 0
  #   to_port          = 0
  #   protocol         = "-1"
  #   cidr_blocks      = ["0.0.0.0/0"]
  #   ipv6_cidr_blocks = ["::/0"]
  #   security_groups  = ["${aws_security_group.govtech_counter_alb_sg.id}"]
  # }
}

resource "aws_autoscaling_group" "govtech_counter_auto_scaling_group" {
  # availability_zones        = var.asg_availability_zones
  desired_capacity          = var.asg_desired_capacity
  max_size                  = var.asg_max_size
  min_size                  = var.asg_min_size
  health_check_grace_period = var.asg_health_check_grace_period
  health_check_type         = var.asg_health_check_type
  force_delete              = var.asg_force_delete
  vpc_zone_identifier       = ["${aws_subnet.govtech_counter_private_subnet_1a.id}"]

  launch_template {
    id      = aws_launch_template.govtech_counter_launch_template.id
    version = var.launch_template_version
  }
}

# Download asset(s) from S3 bucket.