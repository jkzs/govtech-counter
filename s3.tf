resource "aws_s3_bucket" "govtech_counter_assets_bucket" {
  bucket = var.s3_assets_bucket_name
  acl    = var.s3_assets_bucket_acl

  versioning {
    enabled = var.s3_assets_bucket_versioning
  }

  logging {
    target_bucket = aws_s3_bucket.govtech_counter_assets_log_bucket.id
    target_prefix = var.s3_assets_bucket_log_target_prefix
  }
}

resource "aws_s3_bucket" "govtech_counter_assets_log_bucket" {
  bucket = var.s3_assets_log_bucket_name
  acl    = var.s3_assets_log_bucket_acl
}

resource "aws_s3_bucket_object" "govtech_counter_homepage_object" {
  bucket = aws_s3_bucket.govtech_counter_assets_bucket.id
  key    = var.govtech_counter_homepage_object_key
  source = var.govtech_counter_homepage_object_source

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("${var.govtech_counter_homepage_object_source}")
}